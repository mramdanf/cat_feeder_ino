#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <Time.h>
#include <TimeAlarms.h>

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;

String base_url = "http://192.168.1.16/cat_feeder/api/";
int alat_update_interval; // Update status alat interval (detik)
int makanan_id; // Id makanan, diambil dari server
int dispend_quantity; // Banyaknya makanan yang harus didispend

void setup() {

    Serial.begin(115200);

    // Delay sebelum konek ke wifi
    for(uint8_t t = 4; t > 0; t--) {
        Serial.printf("[SETUP] WAIT %d...\n", t);
        Serial.flush();
        delay(1000);
    }

    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP("Metrasat", "M3TR45AT");

    // Delay sebelum membuat koneksi ke internet
    Serial.println("Inisialisasi sistem...");
    delay(1000);

    // Get jadwal makan dan config alat from server
    get_refresh_data();    
}

void loop() {
    digitalClockDisplay();
    Alarm.delay(1000);
}

void digitalClockDisplay() {
    // digital clock display of the time
    Serial.print(hour());
    printDigits(minute());
    printDigits(second());
    Serial.println(); 
}

void printDigits(int digits) {
  
    Serial.print(":");
    
    if(digits < 10)
        Serial.print('0');

    Serial.print(digits);
}

void get_refresh_data() {
    // Mengambil data jadwal makan dan menerapkan
    // jadwal ke library alarm
    init_jadwal_makan();

    delay(1000);

    // Get data config alat (interval update alat)
    init_config_alat();
}

