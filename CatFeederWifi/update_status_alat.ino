
void update_status_alat() {
  
    // Try connecting wifi
    while(WiFiMulti.run() != WL_CONNECTED) {
        Serial.println("Connecting wifi...");
        delay(5000);
    }

    String data = "id=1&status=ON";

    HTTPClient http;
    http.begin(base_url+"alat/update");
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    int httpCode = http.POST(data);
    
    Serial.println("[HTTP] Post update alat....");

    // HTTP header has been send and Server response header has been handled
    Serial.printf("[HTTP] POST... code: %d\n", httpCode);
    
    if(httpCode > 0) {
        
        // file found at server
        if(httpCode == HTTP_CODE_OK) {
          
            String response = http.getString();

            // Allocate JsonBuffer
            // Use arduinojson.org/assistant to compute the capacity.
            const size_t bufferSize = JSON_OBJECT_SIZE(1) + 40;
            DynamicJsonBuffer jsonBuffer(bufferSize);
        
            // Parse JSON object
            JsonObject& root = jsonBuffer.parseObject(response);
            if (!root.success()) {
                Serial.println("Parsing failed!");
                return;
            }
        
            // Extract values
            const char* message = root["message"];
        
            Serial.println(message);
        }
    }
    else {
        Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    Serial.println("Disconnected.");
    Serial.println("===================================");
}
