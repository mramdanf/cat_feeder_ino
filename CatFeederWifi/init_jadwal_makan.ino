
void init_jadwal_makan() {

  // Try connecting wifi
	while(WiFiMulti.run() != WL_CONNECTED) {
		Serial.println("Connecting wifi...");
		delay(5000);
	}
      
	HTTPClient http;
	http.begin(base_url+"jadwal/get_all");

	// start connection and send HTTP header
	int httpCode = http.GET();

	Serial.println("[HTTP] Get jadwal makan....");

	if(httpCode > 0) {

		// HTTP header has been send and Server response header has been handled
		Serial.printf("[HTTP] GET... code: %d\n", httpCode);

		// file found at server
		if(httpCode == HTTP_CODE_OK) {
			String response = http.getString();

			// Allocate JsonBuffer
			// Use arduinojson.org/assistant to compute the capacity.
			const size_t bufferSize = JSON_ARRAY_SIZE(3) + JSON_OBJECT_SIZE(2) + 3*JSON_OBJECT_SIZE(3) + 190;
			DynamicJsonBuffer jsonBuffer(bufferSize);

			// Parse JSON object
			JsonObject& root = jsonBuffer.parseObject(response);
			if (!root.success()) {
				Serial.println("Parsing failed!");
				return;
			}

      		// Parsing current time
			const char* today = root["today"];
			char* today_copy  = strdup(today);
			char* jam         = strtok(today_copy, ",");
			char* menit       = strtok(NULL, ",");
			char* detik       = strtok(NULL, ",");
			char* tgl         = strtok(NULL, ",");
			char* bln         = strtok(NULL, ",");
			char* thn         = strtok(NULL, ",");

			// Setting current time
			setTime(atoi(jam), atoi(menit), atoi(detik), atoi(tgl), atoi(bln), atoi(thn));

		  // Setting refresh data every 6am (morning)
		  Alarm.alarmRepeat(6,43,0, get_refresh_data);

		  // Parsing jadwal
			JsonArray& jadwals = root["jadwals"];
			Serial.println("jadwal makan");

			// Setting makanan_id, asumsi setiap jadwal 
			// makanannya sama (makanan_id nya sama)
			makanan_id = jadwals[0]["makanan_id"];
  
			for (int i = 0; i < 3; ++i) {

				JsonObject& sch = jadwals[i];

				// Parsing jam makan
				const char* jam_txt = sch["jam_txt"];
				char* jam_txt_copy = strdup(jam_txt);
				int h = atoi(strtok(jam_txt_copy, ","));
				int m = atoi(strtok(NULL, ","));
				int s = atoi(strtok(NULL, ","));

				// Setting jadwal makan sebagai alarm
				Alarm.alarmRepeat(h,m,s, dispend_add_log);

				// Log jadwal makan, untuk memastikan jadwalnya
				// sesuai dengan data di DB
				Serial.print(h);
				printDigits(m);
				printDigits(s);
				Serial.println();
			}
		}
	} 
	else {
		Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
	}

	http.end();
	Serial.println("Disconnected.");
	Serial.println("===================================");
}
