
void init_config_alat() {

    // Try connecting wifi
    while(WiFiMulti.run() != WL_CONNECTED) {
      Serial.println("Connecting wifi...");
      delay(5000);
    }

    HTTPClient http;
    http.begin(base_url+"alat/get?id=1");
  
    // start connection and send HTTP header
    int httpCode = http.GET();

    Serial.println("[HTTP] Get config alat....");

    // HTTP header has been send and Server response header has been handled
    Serial.printf("[HTTP] GET... code: %d\n", httpCode);

    if(httpCode > 0) {
        
        if(httpCode == HTTP_CODE_OK) {
            String response = http.getString();

            // Allocate JsonBuffer
            // Use arduinojson.org/assistant to compute the capacity.
            const size_t bufferSize = JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(5) + 110;
            DynamicJsonBuffer jsonBuffer(bufferSize);
        
            // Parse JSON object
            JsonObject& root = jsonBuffer.parseObject(response);
            if (!root.success()) {
                Serial.println("Parsing failed!");
                return;
            }
        
            // Extract values
            JsonObject& alat = root["alat"];
            alat_update_interval = alat["update_interval"];
        
            Serial.print("Get data alat success, update interval: ");
            Serial.println(alat_update_interval);
        
            // Update status alat every alat_update_interval seconds
            Alarm.timerRepeat(alat_update_interval, update_status_alat);
        }
    }
    else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    Serial.println("Disconnected.");
    Serial.println("===================================");
}
