void add_log_makan(int before_dispend, int after_dispend, int makanan_id) {
  
    // Try connecting wifi
    while(WiFiMulti.run() != WL_CONNECTED) {
        Serial.println("Connecting wifi...");
        delay(5000);
    }

    String data = "before_feed="+String(before_dispend)+"&after_feed="+String(after_dispend)+"&makanan_id="+String(makanan_id);

    HTTPClient http;
    http.begin(base_url+"log_data/add");
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    int httpCode = http.POST(data);
    
    Serial.println("[HTTP] Post log makan....");

    // HTTP header has been send and Server response header has been handled
    Serial.printf("[HTTP] POST... code: %d\n", httpCode);
    
    if(httpCode > 0) {
        
        // file found at server
        if(httpCode == HTTP_CODE_OK) {
          
            String payload = http.getString();

            // Allocate JsonBuffer
            // Use arduinojson.org/assistant to compute the capacity.
            const size_t bufferSize = JSON_OBJECT_SIZE(1) + 40;
            DynamicJsonBuffer jsonBuffer(bufferSize);
        
            // Parse JSON object
            JsonObject& root = jsonBuffer.parseObject(payload);
            if (!root.success()) {
                Serial.println("Parsing failed!");
                return;
            }
        
            // Extract values
            const char* message = root["message"];
        
            Serial.println(message);
        }
    }
    else {
        Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    Serial.println("Disconnected.");
    Serial.println("===================================");
}
